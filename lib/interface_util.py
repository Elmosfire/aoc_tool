import re
from aocd import get_data, submit

with open("SESSION_TOKEN") as file:
    session_token = file.read()


class AocInterface:
    test_overwrite = ""
    def __init__(self, day, year, part):
        self.day = day
        self.year = year
        self.part = part
        self.testcases = []
        
    def download(self):
        return get_data(session_token, self.day, self.year)
    
    def submit(self, mainfunc):
        result = mainfunc(self.download())
        print("solution", result)
        return submit(result, session=session_token, part=self.part, day=self.day, year=self.year)
    
    def register_test_case(self, inp, out):
        self.testcases.append((inp, out))

    def verify(self, mainfunc):
        for inp, out in self.testcases:
            res = mainfunc(inp)
            assert res == out, f"input: \n{inp}\n results in: {res} instead of {out}"
            
    def verify_and_submit(self, mainfunc):
        assert self.testcases, "no testcases provided, will not run"
        self.verify(mainfunc)
        self.submit(mainfunc)
        
        

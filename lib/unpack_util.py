import re
from collections import namedtuple
import pandas as pd

Detector = namedtuple("Detector", "name,left,right,postproc")



INT_DETECTOR = Detector("int", r"(-?\d+)", r"\1", int)
WORD_DETECTOR = Detector("word", r"\b(\w+)\b", r"\1", lambda x:x)
CHAR_DETECTOR = Detector("char", r"(.)", r"\1", lambda x:x)

def exact_detector(value, translate=None):
    if translate is not None:
        return Detector(value, re.escape(value), "ERROR", lambda x:translate)
    else:
        return Detector(value, re.escape(value), re.escape(value), lambda x:x)
    
def exact_dict(d):
    for k,v in d.items():
        yield exact_detector(k, v)

def detectlocs(data, name, left, right=r"\1", postproc=lambda x:x):
    re_comp = re.compile(left)
    for chunkindex, data_chunk in enumerate(data.split("\n\n")):
        for y, line in enumerate(data_chunk.split("\n")):
            for m in re_comp.finditer(line):
                val = m.group(0)
                subbed = re_comp.sub(right, val)
                extract = postproc(subbed)
                yield chunkindex, y, m.start(0), name, val, subbed, extract
                
def detect_link(data):
    for chunkindex, data_chunk in enumerate(data.split("\n\n")):
        for y, line in enumerate(data_chunk.split("\n")):
            yield chunkindex, y, line
                
def detectlocs_glob(data, sys):
    if not sys:
        sys = [CHAR_DETECTOR]
    for tup in sys:
        print("extracting", tup)
        yield from detectlocs(data, *tup)
        


        
        
class CompileToolbox:
    def __init__(self, data, system):
        dtlg = detectlocs_glob(data, system)
        self.df = pd.DataFrame(dtlg, columns=["chunk", "y", "x", "source", "raw", "extract", "value"])
        self.link = {(c, y): v for c,y, v in detect_link(data)}
    def default(self, apply=lambda x:x, combine_line=sum, combine_chunk=sum, combine_final=sum, debug=False):
        if debug:
            for c, chunk in self.df.groupby("chunk"):
                for y, line in chunk.groupby("y"):
                    val = combine_line([apply(next(iter(node["value"]))) for _, node in line.groupby("x")]) 
                    print(c, y, self.link[c, y], val)
            
        return combine_final([
            combine_chunk([
                combine_line(
                    [apply(next(iter(node["value"]))) for _, node in line.groupby("x")]) 
                for _, line in chunk.groupby("y")]) 
            for _, chunk in self.df.groupby("chunk")])
        

        
    
    
            

        
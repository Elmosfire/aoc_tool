import lib.unpack_util as unpack_util
import lib.interface_util as interface_util
from IPython.display import display

            
def build_inject(system, type_, options, debug=True):
    injected = options
    executor = getattr(unpack_util.CompileToolbox, type_)
    def injected_func(data):
        print("executing as", executor, injected)
        toolbox = unpack_util.CompileToolbox(data, system)
        if debug:
            display(toolbox.df)
        return executor(toolbox, **injected)
        
    return injected_func
            
class AOCG:
    def __init__(self, year, day):
        self.system_1 = []
        self.system_2 = []
        self.aoci_1 = interface_util.AocInterface(day, year, "a")
        self.aoci_2 = interface_util.AocInterface(day, year, "b")
    def detector(self, syss, onlypart=None):
        if isinstance(syss, dict):
            syss = list(unpack_util.exact_dict(syss))
        if not onlypart or onlypart == "a":
            self.system_1.extend(syss)
        if not onlypart or onlypart == "b":
            self.system_2.extend(syss)
    def testcase_1(self, left, right):
        self.aoci_1.register_test_case(left, right)
    def testcase_2(self, left, right):
        self.aoci_2.register_test_case(left, right)
    def testcase(self, left, right1, right2):
        self.aoci_1.register_test_case(left, right1)
        self.aoci_2.register_test_case(left, right2)
        
    def part1(self, type_, **options):
        subfunc = build_inject(self.system_1, type_, options, debug=options.get("debug", False))
        self.aoci_1.verify_and_submit(subfunc)
    def part2(self, type_, **options):
        subfunc = build_inject(self.system_2, type_, options, debug=options.get("debug", False))
        self.aoci_2.verify_and_submit(subfunc)